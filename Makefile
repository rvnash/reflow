TARGET_DEVICE=xenon
TARGET_VERSION=1.5.2
EXE=bin/reflow.bin
PROPSFILE=project.properties
SOURCES=$(wildcard src/*.cpp src/*.h src/SparkFun_MCP9600/*.cpp src/SparkFun_MCP9600/*.h) $(PROPSFILE)

# Rules
$(EXE): $(SOURCES)
	mkdir -p bin
	echo $$PATH
	particle compile $(TARGET_DEVICE) --target $(TARGET_VERSION) $^ --saveTo $@

flash: $(EXE)
	particle flash --usb $^

clean:
	rm -rf bin *~ src/*~
