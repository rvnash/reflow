#pragma once

class exp_filter
{
public:
 exp_filter(double a):
   _a(a)
    {
      _oneminusa = 1 - a;
      _lastY = sqrt(-1);
    };

  double filter(double y)
  {
    if (_lastY != _lastY) _lastY = y; // First value signaled by NaN
    double retVal = _a * _lastY + _oneminusa * y;
    _lastY = y;
    return retVal;
  };
  
private:
  double _a, _oneminusa, _lastY;
};

