#ifndef _PID_H_
#define _PID_H_

class PID
{
    public:
        // Kp -  proportional gain
        // Ki -  Integral gain
        // Kd -  derivative gain
        // dt -  loop interval time
        // max - maximum value of manipulated variable
        // min - minimum value of manipulated variable
        // controlableRegion - How much on either side of the setpoint to allow the integral term to work
        PID();
        void init( double dt, double max, double min, double Kp, double Kd, double Ki, double controlableRegion );
        void reset();

        // Returns the manipulated variable given a setpoint and current process value
        double calculate( double setpoint, double pv, double &p, double &i, double &d );

    private:
        double _dt;
        double _max;
        double _min;
        double _Kp;
        double _Kd;
        double _Ki;
        double _pre_error;
        double _integral;
        double _controlableRegion;
};

#endif
