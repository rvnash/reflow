#include <stdint.h>

struct Profile
{
  char name[30];
  double preheatPhase_StartTemp;
  double preheatPhase_EndTemp;
  double preheatPhase_Rate;
  double soakPhase_Duration;
  double rampToPeak_Rate;
  double reflowPhase_Temp;
  double reflowPhase_Duration;
  double coolingPhase_EndTemp;
  double coolingPhase_Rate;
};

class StoredProfiles
{
public:
  StoredProfiles();
  void init();
  int8_t profileCount();
  void listProfiles();
  void saveProfile(int8_t slot, Profile p);
  int8_t getDefault();
  void setDefault(int8_t slot);
  bool deleteProfile(int8_t slot);
  bool printProfile(int8_t slot);
  bool addProfile();
  Profile getProfile(int8_t slot);
private:
  int8_t iCount,iDefault;
};

class ReflowProfile
{
public:
  ReflowProfile();
  void init(Profile &p);
  void start();
  void stop();
  bool running();
  uint32_t currentTime();
  uint32_t timeInPhase();
  uint32_t timeUntilNextPhase();
  const char *phaseName();
  double getSetTemperature();
private:
  void updateToNow();
  double _startTemp, _preheatPhase_EndTemp, _preheatPhase_Rate,
         _soakPhase_Duration,
         _rampToPeak_Rate,
         _reflowPhase_Temp, _reflowPhase_Duration,
         _coolingPhase_EndTemp, _coolingPhase_Rate;
  uint32_t _startTime;
  uint32_t _soakStart, _rampToPeakStart, _reflowStart, _coolStart;
  uint32_t _timeInPhase;
  uint32_t _timeUntilNextPhase;
  double _setTemperature;
  const char *_phaseName;
};
