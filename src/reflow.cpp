/*
reflow
Copyright (C) 2020 Richard Nash
All rights reserved.

This is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <SparkFun_MCP9600.h>
MCP9600 tempSensor;
#include "InLoopPeriodicCallback.h"
#include "PID.h"
#include "exp_filter.h"
#include "ReflowProfile.h"

#define RELAY D4
#define REFLOW_VERSION "1.0"

STARTUP(System.enableFeature(FEATURE_RETAINED_MEMORY));
SYSTEM_MODE(MANUAL);

float currentTemp = 20.0f;
float unfilteredCurrentTemp = 20.0f;

bool getTemperature(unsigned long);
bool printStatus(unsigned long);
bool control(unsigned long);
InLoopPeriodicCallback cbGetTemperature(getTemperature, 100, 100, 100);
InLoopPeriodicCallback cbPrintStatus(printStatus, 1000, 1000, 1000);
InLoopPeriodicCallback cbControlLoop(control, 1000, 1000, 1000);

bool running = false;
PID pid;
exp_filter f(0.93);

StoredProfiles profileStorage;
ReflowProfile reflowprofile;


// This modifies the performance of pressing the SETUP button
// Pressing < .5 seconds enters dfu state (for reprogramming)
void button_handler(system_event_t event, int duration)
{
  if (duration == 0) {
    // Press of button
    return;
  }
  if (duration > 500) {
  } else {
    digitalWrite(RELAY,LOW);
    Serial.printf("Enter dfu\n");
    delay(100);
    System.dfu();
  }
}

void shortHelp()
{
  Serial.printf( "\nReflow Oven (" REFLOW_VERSION ") | Hit a key (L,C,M,↑,↓,X,A,R,O,F,U,?) : ");
}

void help()
{
  Serial.printf(
    "Profile Management:\n"
    "    L - List available profiles\n"
    "    C - List current profile\n"
    "    M - Modify current profile\n"
    "    ↑ / ↓  - Change the current profile\n"
    "    X - Delete the current\n"
    "    A - Add a profile\n"
    "Commands:\n"
    "    R - Run / Stop the current profile\n"
    "    O - Manually turn on heating element\n"
    "    F - Manually turn off heating element\n"
    "System Commands:\n"
    "    U - Enter dfu mode for reprogramming\n"
    "    ? - Help\n"
  );
}

void getCurrentProfile()
{
  Profile p = profileStorage.getProfile(profileStorage.getDefault());
  reflowprofile.init(p);
}

void setup()
{
  // Make sure the relay is off
  pinMode(RELAY,OUTPUT);
  digitalWrite(RELAY,LOW);

  // Bookkeeping
  Mesh.off();
  System.on(button_status, button_handler);

  // Start serial communication and wait for a connection
  Serial.begin();       // USB
  waitFor(Serial.isConnected, 4000);

  // Start the temperature sensor
  Wire.begin();
  Wire.setClock(100000);
  tempSensor.begin();       // Uses the default address (0x60) for SparkFun Thermocouple Amplifier
  tempSensor.begin(0x60); // Default address (0x66) for SparkX Thermocouple Amplifier
  // Check if the sensor is connected
  if(!tempSensor.isConnected()){
    Serial.printf("Device did not acknowledge! Freezing\n");
    while(1); //hang forever
  }
  // Check if the Device ID is correct
  if(!tempSensor.checkDeviceID()){
    Serial.printf("Device ID is not correct! Freezing\n");
    while(1); //hang forever
  }
  cbGetTemperature.init();
  cbPrintStatus.init();
  cbControlLoop.init();
  pid.init( 1.0, 1000.0, -1000.0, 1.0, 9.0, 0.01, 8.0 );

  profileStorage.init();
  Profile p = profileStorage.getProfile(profileStorage.getDefault());
  reflowprofile.init(p);
  getCurrentProfile();

  Serial.printf("\n\nWelcome to the Reflow Oven Version: " REFLOW_VERSION "\n"
                    "---------------------------------------\n\n");
  Serial.printf("Current profile is %d of %d\n", profileStorage.getDefault(), profileStorage.profileCount());
  profileStorage.printProfile(profileStorage.getDefault());
  shortHelp();
}

void getCurrentTemp()
{
  unfilteredCurrentTemp = tempSensor.getThermocoupleTemp();
  currentTemp = f.filter(unfilteredCurrentTemp);
}

bool getTemperature(unsigned long)
{
  getCurrentTemp();
  return true;
}

bool printStatus(unsigned long)
{
  //if (!running) Serial.printf("%5.1f, %5.1f\n", currentTemp, unfilteredCurrentTemp);
  return true;
}

bool control(unsigned long)
{
  if (!running) return true;
  double p,i,d;
  double setTemp = reflowprofile.getSetTemperature();
  double output = pid.calculate(setTemp, currentTemp, p, i, d);
  if (output < 0 ) {
    digitalWrite(RELAY,LOW);
  } else {
    digitalWrite(RELAY,HIGH);
  }
  Serial.printf("%3d, %5.1f, %5.1f, %5.1f, %5.2lf, %5.2lf, %5.2lf, %5.2lf, %s, %s\n",
                reflowprofile.currentTime()/1000, setTemp, currentTemp, unfilteredCurrentTemp,
                output, p, i, d, reflowprofile.phaseName(), digitalRead(RELAY) ? "ON" : "OFF");
  return true;
}


void loop()
{
  cbGetTemperature.loop();
  cbPrintStatus.loop();
  cbControlLoop.loop();
}


// Input from the console
bool escape=false;
bool ansi=false;
#define ANSI_NONE 0
#define ANSI_UPARROW 1
#define ANSI_DOWNARROW 2
#define ANSI_UNKNOWN -1
int8_t ansiCode=ANSI_NONE;
void processAnsi(char c)
{
  ansiCode=ANSI_NONE;
  if (escape) {
    if (ansi) {
      switch (c) {
        case 'A':
          ansiCode = ANSI_UPARROW;
        break;
        case 'B':
          ansiCode = ANSI_DOWNARROW;
        break;
        default:
        // Code not currently recognized
          ansiCode = ANSI_UNKNOWN;
        break;
      }
      ansi = false;
      escape = false;
    } else {
      if (c == '[')
        ansi = true;
      else {
        ansi = false;
        escape = false;
      }
    }
  } else {
    if (c == 27)
      escape = true;
    else {
      ansi = false;
      escape = false;
    }
  }
}

bool enteringLine = false;
char line[1024];
int iOnChar;

void gotLine()
{
  char name[30],a1[20],a2[20],a3[20],a4[20],a5[20];
  float riseRate,soakTemp,soakDuration,reflowTemp,reflowDuration;
  enteringLine = false;
  int params = sscanf(line,"%29[^,],%19[^,],%19[^,],%19[^,],%19[^,],%19[^,]",name,a1,a2,a3,a4,a5);
  if (params != 6) {
    Serial.printf("Expected 6 paramaters, parsed %d.\n", params);
  } else {
    char *endptr;
    riseRate = strtof(a1,&endptr);
    if (*endptr != '\0') {
      Serial.printf("Failed to parse %s\n", a1);
      return;
    }
    soakTemp = strtof(a2,&endptr);
    if (*endptr != '\0') {
      Serial.printf("Failed to parse %s\n", a2);
      return;
    }
    soakDuration = strtof(a3,&endptr);
    if (*endptr != '\0') {
      Serial.printf("Failed to parse %s\n", a3);
      return;
    }
    reflowTemp = strtof(a4,&endptr);
    if (*endptr != '\0') {
      Serial.printf("Failed to parse %s\n", a4);
      return;
    }
    reflowDuration = strtof(a5,&endptr);
    if (*endptr != '\0') {
      Serial.printf("Failed to parse %s\n", a5);
      return;
    }
    Profile p = profileStorage.getProfile(profileStorage.getDefault());
    strcpy(p.name,name);
    p.preheatPhase_Rate = riseRate;
    p.rampToPeak_Rate = riseRate;
    p.coolingPhase_Rate = -riseRate;
    p.preheatPhase_EndTemp = soakTemp;
    p.soakPhase_Duration = soakDuration;
    p.reflowPhase_Temp = reflowTemp;
    p.reflowPhase_Duration = reflowDuration;
    profileStorage.saveProfile(profileStorage.getDefault(),p);
    Serial.printf("\nProfile updated\n");
    profileStorage.printProfile(profileStorage.getDefault());
    getCurrentProfile();
    shortHelp();
  }
}

void processLine(char c)
{
  if (c == '\r' || c == '\n') {
    gotLine();
  } if (c == 127) {
    if (iOnChar == 0) return;
    Serial.printf("\b \b");
    iOnChar--;
    line[iOnChar] = '\0';
  } else {
    if (iOnChar >= 1023) return;
    line[iOnChar++] = c;
    line[iOnChar] = '\0';
    Serial.write(c);
  }
}


void serialEvent()
{
  while (Serial.available()) {
    char c = Serial.read();
    if (running) {
      digitalWrite(RELAY,LOW);
      reflowprofile.stop();
      Serial.printf("RUNNING CANCELED!\n");
      running = false;
      shortHelp();
      return;
    }
    if (enteringLine) {
      processLine(c);
      continue;
    }
    processAnsi(c);
    if (!ansi && !escape && ansiCode == ANSI_NONE) {
      switch (c) {
        case 'L': case 'l':
          Serial.printf("%c\n",c);
          profileStorage.listProfiles();
          shortHelp();
          break;
        case 'M': case 'm':
          Serial.printf("%c\n",c);
          Serial.printf("Separated by commas, enter...\nName, Rise Rate, SOAK Temp, SOAK Duration, Reflow Temp, Reflow Duration\n> ");
          enteringLine = true;
          iOnChar = 0;
          line[iOnChar] = '\0';
          break;
        case 'C': case 'c':
          Serial.printf("%c\n",c);
          Serial.printf("Current profile is %d\n", profileStorage.getDefault());
          profileStorage.printProfile(profileStorage.getDefault());
          shortHelp();
          break;
        case 'X': case 'x':
          Serial.printf("%c\n",c);
          if (profileStorage.deleteProfile(profileStorage.getDefault()))
            profileStorage.listProfiles();
          getCurrentProfile();
          shortHelp();
          break;
        case 'A': case 'a':
          Serial.printf("%c\n",c);
          if (profileStorage.addProfile())
            profileStorage.listProfiles();
          getCurrentProfile();
          shortHelp();
          break;
        case 'R': case 'r':
          Serial.printf("%c\n",c);
          running = true;
          getCurrentProfile();
          reflowprofile.start();
          pid.reset();
          Serial.printf("RUNNING profile:\n");
          profileStorage.printProfile(profileStorage.getDefault());
          Serial.printf("Any key cancels\n");
          break;
        case 'O': case 'o':
          Serial.printf("%c\n",c);
          Serial.printf("Oven On\n");
          digitalWrite(RELAY,HIGH);
          shortHelp();
          break;
        case 'F': case 'f':
          Serial.printf("%c\n",c);
          Serial.printf("Oven Off\n");
          digitalWrite(RELAY,LOW);
          shortHelp();
          break;
        case 'U': case 'u':
          digitalWrite(RELAY,LOW);
          Serial.printf("%c\n",c);
          Serial.printf("Entering dfu mode\n");
          delay(200);
          System.dfu();
          break;
        case '?': case 'h': case 'H':
          Serial.printf("%c\n",c);
          Serial.printf("\n");
          help();
          shortHelp();
          break;
        default:
          Serial.printf("\n");
          Serial.printf("Unknown command: '%c' (%d)\n", c, c);
          shortHelp();
          break;
      }
    } else {
      switch (ansiCode) {
        int8_t slot;
        case ANSI_UPARROW:
          Serial.printf("Select Previous\n",c);
          slot = profileStorage.getDefault();
          if (slot > 1) {
            profileStorage.setDefault(slot-1);
            profileStorage.listProfiles();
          }
          shortHelp();
        break;
        case ANSI_DOWNARROW:
          Serial.printf("Select Next\n",c);
          slot = profileStorage.getDefault();
          if (slot < profileStorage.profileCount()) {
            profileStorage.setDefault(slot+1);
            profileStorage.listProfiles();
          }
          shortHelp();
        break;
        case ANSI_UNKNOWN:
          Serial.printf("Unknown ANSI escape sequence\n");
          shortHelp();
        break;
      }
    }
  }
}
