#include "ReflowProfile.h"
#include <Particle.h>

ReflowProfile::ReflowProfile()
{
}

void ReflowProfile::init(Profile &p)
{
  _startTemp = p.preheatPhase_StartTemp;
  _preheatPhase_EndTemp = p.preheatPhase_EndTemp;
  _preheatPhase_Rate = p.preheatPhase_Rate;
  _soakPhase_Duration = p.soakPhase_Duration;
  _rampToPeak_Rate = p.rampToPeak_Rate;
  _reflowPhase_Temp = p.reflowPhase_Temp;
  _reflowPhase_Duration = p.reflowPhase_Duration;
  _coolingPhase_EndTemp = p.coolingPhase_EndTemp;
  _coolingPhase_Rate = p.coolingPhase_Rate;
  _startTime = 0; // Not started
  _soakStart = (uint32_t)(1000.0 * (_preheatPhase_EndTemp - _startTemp) / _preheatPhase_Rate);
  _rampToPeakStart = _soakStart + (uint32_t)(1000.0 *  _soakPhase_Duration);
  _reflowStart = _rampToPeakStart + (uint32_t)(1000.0 * (_reflowPhase_Temp - _preheatPhase_EndTemp) / _rampToPeak_Rate);
  _coolStart = _reflowStart + (uint32_t)(1000.0 *  _reflowPhase_Duration);
}

void ReflowProfile::start()
{
  _startTime = millis();
  updateToNow();
}

void ReflowProfile::stop()
{
  _startTime = 0;
}

bool ReflowProfile::running()
{
  return (_startTime != 0);
}

uint32_t ReflowProfile::currentTime()
{
  return millis() - _startTime;
}

uint32_t ReflowProfile::timeInPhase()
{
  updateToNow();
  return _timeInPhase;
}

uint32_t ReflowProfile::timeUntilNextPhase()
{
  updateToNow();
  return _timeUntilNextPhase;
}

const char *ReflowProfile::phaseName()
{
  updateToNow();
  return _phaseName;
}

double ReflowProfile::getSetTemperature()
{
  updateToNow();
  return _setTemperature;
}

void ReflowProfile::updateToNow()
{
  uint32_t now = currentTime();
  if (now < _soakStart) { // Preheat phase
    _phaseName = "PREHEAT";
    _timeInPhase = now;
    _timeUntilNextPhase = _soakStart - now;
    _setTemperature = _startTemp + (_preheatPhase_Rate * (((double)_timeInPhase)/1000.0));
  } else if (now < _rampToPeakStart) { // Soak phase
    _phaseName = "SOAK";
    _timeInPhase = now - _soakStart;
    _timeUntilNextPhase = _rampToPeakStart - now;
    _setTemperature = _preheatPhase_EndTemp;
  } else if (now < _reflowStart) { // Ramp to peak phase
    _phaseName = "RAMP_TO_PEAK";
    _timeInPhase = now - _rampToPeakStart;
    _timeUntilNextPhase = _reflowStart - now;
    _setTemperature = _preheatPhase_EndTemp + (_rampToPeak_Rate * (((double)_timeInPhase)/1000.0));
  } else if (now < _coolStart) { // Reflow phas
    _phaseName = "REFLOW";
    _timeInPhase = now - _reflowStart;
    _timeUntilNextPhase = _coolStart - now;
    _setTemperature = _reflowPhase_Temp;
  } else { // Cooling phase
    _phaseName = "COOL";
    _timeInPhase = now - _coolStart;
    _timeUntilNextPhase = 0;
    _setTemperature = _reflowPhase_Temp + (_coolingPhase_Rate * (((double)_timeInPhase)/1000.0));
    if (_setTemperature < _coolingPhase_EndTemp) _setTemperature = _coolingPhase_EndTemp;
  }
}

StoredProfiles::StoredProfiles()
{
}

#define FORMAT_SIGNATURE (uint64_t)0xfacedeadfacedead
#define MAX_PROFILES 10
#define LOC_OF(i) 9+sizeof(Profile)*(i)

void StoredProfiles::init()
{
  uint64_t signature;
  Profile p;

  EEPROM.get(0,signature);
  if (signature != FORMAT_SIGNATURE) {
    Serial.printf("FORMATTING EEPROM\n");
    // Format the EEPROM
    EEPROM.put(0,FORMAT_SIGNATURE);
    EEPROM.put(8,(int8_t)0); // Default
    strcpy(p.name, "Sn42/Bi57/Ag1.0 Solder");
    p.preheatPhase_StartTemp = 25.0;
    p.preheatPhase_EndTemp = 120.0;
    p.preheatPhase_Rate = 2.0;
    p.soakPhase_Duration = 150.0;
    p.rampToPeak_Rate = 3.0;
    p.reflowPhase_Temp = 165.0;
    p.reflowPhase_Duration = 90.0;
    p.coolingPhase_EndTemp = 20.0;
    p.coolingPhase_Rate = -3.0;
    EEPROM.put(LOC_OF(0),p);
    bzero(&p,sizeof(p));
    for (int8_t i = 1; i < MAX_PROFILES; i++) {
      EEPROM.put(LOC_OF(i),p);
    }
    iDefault = 0;
    iCount = 1;
  } else {
    EEPROM.get(8,iDefault);
    iCount = MAX_PROFILES;
    for (int8_t i = 0; i < MAX_PROFILES; i++) {
      EEPROM.get(LOC_OF(i),p);
      if (p.name[0] == 0) {
        iCount = i;
        break;
      }
    }
    if (iDefault < 0) {
      iDefault = 0;
      EEPROM.put(8,iDefault);
    }
  }
}

bool StoredProfiles::addProfile()
{
  if (iCount >= MAX_PROFILES) {
    Serial.printf("Already at maximum profiles (%d)\n", MAX_PROFILES);
    return false;
  }
  Profile p;
  strcpy(p.name, "Sn42/Bi57/Ag1.0 Solder");
  p.preheatPhase_StartTemp = 25.0;
  p.preheatPhase_EndTemp = 120.0;
  p.preheatPhase_Rate = 2.0;
  p.soakPhase_Duration = 150.0;
  p.rampToPeak_Rate = 3.0;
  p.reflowPhase_Temp = 165.0;
  p.reflowPhase_Duration = 90.0;
  p.coolingPhase_EndTemp = 20.0;
  p.coolingPhase_Rate = -3.0;
  EEPROM.put(LOC_OF(iCount),p);
  iCount++;
  iDefault = iCount-1;
  EEPROM.put(8,iDefault);
  Serial.printf("Added profile %d.\n", iCount);
  return true;
}

int8_t StoredProfiles::profileCount()
{
  return iCount;
}

bool StoredProfiles::printProfile(int8_t slot)
{
  if (slot < 1 || slot > iCount) {
    Serial.printf("Profile (%d) has to be between 1 and %d\n", slot, iCount + 1);
    return false;
  }
  Profile p;
  EEPROM.get(LOC_OF(slot-1),p);
  Serial.printf("  Name: %s\n", p.name);
  Serial.printf("  Temp rise rate: %.1f°C/sec\n", p.preheatPhase_Rate);
  Serial.printf("  SOAK Temp: %.0f°C\n", p.preheatPhase_EndTemp);
  Serial.printf("  SOAK Duration: %.0f sec\n", p.soakPhase_Duration);
  Serial.printf("  REFLOW Temp: %.0f°C\n", p.reflowPhase_Temp);
  Serial.printf("  REFLOW Duration: %.0f sec\n", p.reflowPhase_Duration);
  return true;
}

void StoredProfiles::listProfiles()
{
  for (int8_t i = 0; i < iCount; i++) {
    Serial.printf("Profile %d", i+1);
    if (iDefault == i) {
      Serial.printf(" (Current)\n");
    } else {
      Serial.printf("\n");
    }
    printProfile(i+1);
  }
}

void StoredProfiles::saveProfile(int8_t slot, Profile p)
{
  if (slot >= iCount+1) {
    Serial.printf("Profile (%d) has to be between 1 and %d\n", slot, iCount + 1);
    return;
  }
  if (slot > MAX_PROFILES) {
    Serial.printf("Maximum of %d profiles\n", MAX_PROFILES);
    return;
  }
  EEPROM.put(LOC_OF(slot-1),p);
  if (slot > iCount) iCount++;
  if (iDefault == -1) setDefault(slot);
}

int8_t StoredProfiles::getDefault()
{
  return iDefault+1;
}

void StoredProfiles::setDefault(int8_t slot)
{
  if (slot < 1 || slot > iCount) {
    Serial.printf("Profile (%d) has to be between 1 and %d\n", slot, iCount + 1);
    return;
  }
  iDefault = slot-1;
  EEPROM.put(8,iDefault);
}

bool StoredProfiles::deleteProfile(int8_t slot)
{
  Profile p;
  if (iCount == 1) {
    Serial.printf("Cannot delete the last profile.\n");
    return false;
  }
  if (slot < 1 || slot > iCount) {
    Serial.printf("Profile (%d) has to be between 1 and %d\n", slot, iCount + 1);
    return false;
  }
  int8_t iProf = slot-1;
  Serial.printf("Deleting %d, iProf=%d, iDefault=%d\n",slot,iProf,iDefault);
  if (iDefault > iProf || iDefault == iCount-1) {
    iDefault -= 1;
    EEPROM.put(8,iDefault);
  }
  Serial.printf("New iDefault=%d\n",iDefault);
  int8_t i;
  for (i = iProf; i < iCount-1; i++) {
    EEPROM.get(LOC_OF(i+1),p);
    EEPROM.put(LOC_OF(i), p);
  }
  bzero(&p,sizeof(Profile));
  EEPROM.put(LOC_OF(i), p);
  Serial.printf("Deleted Profile %d\n", slot);
  iCount--;
  return true;
}

Profile StoredProfiles::getProfile(int8_t slot)
{
  Profile p;
  bzero(&p,sizeof(Profile));
  if (slot < 1 || slot > iCount) {
    Serial.printf("Profile (%d) has to be between 1 and %d\n", slot, iCount + 1);
    return p;
  }
  int8_t iProf = slot-1;
  EEPROM.get(LOC_OF(iProf),p);
  return p;
}
